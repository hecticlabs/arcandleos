CC = arm-none-eabi-gcc
LD = arm-none-eabi-ld

CFLAGS = -mcpu=cortex-m0plus -mthumb -c -O3 -ffunction-sections -fdata-sections -s
LDFLAGS = -nostdlib --gc-sections -s -z max-page-size=0x1000

all: main.elf

main.o: main.c
	$(CC) $(CFLAGS) -o $@ $^

main.elf: main.o
	$(LD) $(LDFLAGS) -o $@ $^

.PHONY: all
